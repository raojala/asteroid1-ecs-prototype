package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"net"
	socketserver "test/src/SocketServer"
	"time"

	"google.golang.org/protobuf/proto"
)

type NetworkSystem struct {
	Server *socketserver.SockServer
}

var NSystem *NetworkSystem = RegisterNetworkSystem()

func RegisterNetworkSystem() *NetworkSystem {
	N := &NetworkSystem{}
	N.Server = &socketserver.SockServer{}
	N.Server.ClientPool = make(map[net.Conn]int)
	// ECS.RegisterSystem(N)

	if err := N.Server.StartListener(); err != nil {
		log.Fatal(err)
	}

	ExitGuard.Add(1)
	go func() {
		defer ExitGuard.Done()

		for IsRunning {
			err := N.Server.AcceptConnection()
			if err != nil {
				log.Println(err)
				break
			}
		}

		fmt.Println("exit listener loop")
	}()

	ExitGuard.Add(1)
	go func() {
		defer ExitGuard.Done()

		// TODO:: koodaa käyttämään channelia ja lähettämään vaan muuttunu data

		for IsRunning {
			time.Sleep(100 * time.Millisecond)

			if len(N.Server.ClientPool) > 0 {

				var err error

				out, err := proto.Marshal(ECS)
				if err != nil {
					log.Fatal(err)
				}

				packetSize := new(bytes.Buffer)
				err = binary.Write(packetSize, binary.LittleEndian, int32(len(out)))
				if err != nil {
					fmt.Println(err)
				}

				for conn := range N.Server.ClientPool {

					// send following packet size
					err = SocketSendAll(conn, packetSize.Bytes())
					if err == net.ErrClosed {
						conn.Close()
						delete(N.Server.ClientPool, conn)
					}

					// send packet
					err = SocketSendAll(conn, out)
					if err == net.ErrClosed {
						conn.Close()
						delete(N.Server.ClientPool, conn)
					}
				}
			}
		}

		fmt.Println("exit sender loop")
	}()

	return N
}

func SocketSendAll(conn net.Conn, bytes []byte) error {

	var err error
	var count int = len(bytes)
	var received int = 0

	for received < count {
		received, err = conn.Write(bytes)
		if err == net.ErrClosed {
			return err
		}
	}

	return nil
}
