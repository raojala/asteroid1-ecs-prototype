package main

import (
	"test/src/IDManager"
	"test/src/Protocol"
)

type EntitySystem interface {
	Update(*Protocol.Ecs)
	RegisterEntity(uint32)
}

var Systems []EntitySystem = make([]EntitySystem, 0)

var ECS *Protocol.Ecs = InitECS()

func InitECS() *Protocol.Ecs {

	ecs := &Protocol.Ecs{}
	ecs.Entities = make(map[uint32]*Protocol.Entity)
	ecs.Transforms = make(map[uint32]*Protocol.Transform)
	ecs.CircleColliders = make(map[uint32]*Protocol.CircleCollider)

	return ecs
}

// NewEntity : Creates a handle to a collection of components and returns the handle
// Handle is a unique uint32 generated with google/uuid
func NewEntity() uint32 {
	entity := &Protocol.Entity{}
	entity.ID = IDManager.NewID()
	entity.Components = make([]uint32, 0)
	ECS.Entities[entity.ID] = entity
	return entity.ID
}

// AddComponentToEntity : Adds a component (struct) to the entity.
func AddComponentToEntity(ecs *Protocol.Ecs, entityID uint32, component interface{}, TypeId uint32) {

	// each component type requires its own case before generics are implemented in late 2021
	switch TypeId {
	case TRANSFORM_TYPE_ID:
		ecs.Transforms[entityID] = component.(*Protocol.Transform)
	case CIRCLE_COLLIDER_TYPE_ID:
		ecs.CircleColliders[entityID] = component.(*Protocol.CircleCollider)
	}
	ecs.Entities[entityID].Components = append(ecs.Entities[entityID].Components, TypeId)
}

func UpdateSystems(ecs *Protocol.Ecs) {
	for _, system := range Systems {
		system.Update(ecs)
	}
}

func RegisterSystem(system EntitySystem) {
	Systems = append(Systems, system)
}
