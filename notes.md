# notes

## test 1

```Go
type KeyValue struct {
    ComponentTypeID uint32
    Component       interface{}
}

const TEST_A_TYPE_ID uint32 = 1
const TEST_B_TYPE_ID uint32 = 2

type TestA struct {
    Nakki1 string
}

type TestB struct {
    Nakki2 string
}

func Main() {

    entities := make(map[uint32]*KeyValue)
    ID1 := IDManager.NewID()
    ID2 := IDManager.NewID()
    STR1 := TestA{Nakki1: "daadaa"}
    STR2 := TestB{Nakki2: "daadaa"}
    entities[ID1] = &KeyValue{ComponentTypeID: TEST_A_TYPE_ID, Component: &STR1}
    entities[ID2] = &KeyValue{ComponentTypeID: TEST_B_TYPE_ID, Component: &STR2}
    aa := entities[ID2].Component.(*TestB)
    aa.Nakki2 = "vaihdettu teksti"
}
```