package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"test/src/Protocol"
	vectormath "test/src/VectorMath"
	"time"
)

var IsRunning bool = true
var ExitGuard sync.WaitGroup

var TimeStep time.Duration = time.Nanosecond * 33000000
var DeltaTime float32

func main() {

	Signals := make(chan os.Signal, 1)
	signal.Notify(Signals, syscall.SIGINT, syscall.SIGTERM)
	SignalReceived := make(chan bool, 1)
	go func() {
		<-Signals
		SignalReceived <- true
	}()

	Start()

	// server mainloop
	for IsRunning {
		start := time.Now()
		select {
		case <-SignalReceived:
			IsRunning = false
		case <-time.After(TimeStep):
			Update()
			//fmt.Println(time.Since(start))
		}
		DeltaTime = 1 / float32(time.Since(start))
	}

	err := NSystem.Server.CloseServer()
	if err != nil {
		log.Println(err)
	}
	ExitGuard.Wait()
	fmt.Println("bye!")
}

func Start() {
	for i := 0; i < 1000; i++ {
		ast := NewEntity()
		t := &Protocol.Transform{}
		t.Location = vectormath.RandomUnitVector3()
		t.Direction = vectormath.RandomUnitVector3()
		AddComponentToEntity(ECS, ast, t, TRANSFORM_TYPE_ID)
		MSystem.RegisterEntity(ast)
	}
}

func Update() {
	UpdateSystems(ECS)

	// update clients
}
