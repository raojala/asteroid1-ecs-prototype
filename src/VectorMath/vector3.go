package vectormath

import (
	"math"
	"math/rand"
	"test/src/Protocol"
)

// type Vector3 struct {
// 	X, Y, Z float32
// }

func NewVector3(x, y, z float32) *Protocol.Vector3 {
	vec3 := &Protocol.Vector3{}
	vec3.X = x
	vec3.Y = y
	vec3.Z = z
	return vec3
}

func AddVectors(vectors ...*Protocol.Vector3) *Protocol.Vector3 {
	v := NewVector3(0, 0, 0)
	for _, V2 := range vectors {
		v.X += V2.X
		v.Y += V2.Y
		v.Z += V2.Z
	}
	return v
}

func SubVectors(V1 *Protocol.Vector3, vectors ...*Protocol.Vector3) {
	for _, V2 := range vectors {
		V1.X -= V2.X
		V1.Y -= V2.Y
		V1.Z -= V2.Z
	}
}

func Magnitude(V1 *Protocol.Vector3) float32 {
	return float32(math.Sqrt(float64(V1.X*V1.X + V1.Y*V1.Y + V1.Z*V1.Z)))
}

func Normalize(V1 *Protocol.Vector3) *Protocol.Vector3 {

	v1Len := Magnitude(V1)
	return NewVector3(V1.X/v1Len, V1.Y/v1Len, V1.Z/v1Len)
}

func Divide(V1 *Protocol.Vector3, V2 *Protocol.Vector3) *Protocol.Vector3 {
	return NewVector3(V1.X/V2.X, V1.Y/V2.Y, V1.Z/V2.Z)
}

func CrossProduct(V1 *Protocol.Vector3, V2 *Protocol.Vector3) *Protocol.Vector3 {
	return NewVector3(V1.Y*V2.Z-V1.Z*V2.Y, -(V1.X*V2.Z - V1.Z*V2.X), V1.X*V2.Y-V1.Y*V2.X)
}

func DotProduct(V1 *Protocol.Vector3, V2 *Protocol.Vector3) float32 {
	return V1.X*V2.X + V1.Y*V2.Y + V1.Z*V2.Z
}

var Vec3Forward *Protocol.Vector3 = NewVector3(1, 0, 0)
var Vec3Backward *Protocol.Vector3 = NewVector3(-1, 0, 0)
var Vec3Upward *Protocol.Vector3 = NewVector3(0, 1, 0)
var Vec3Downward *Protocol.Vector3 = NewVector3(0, -1, 0)

func RandomUnitVector3() *Protocol.Vector3 {
	var min float32 = -10
	var max float32 = 10
	X := min + rand.Float32()*(max-min)
	Y := min + rand.Float32()*(max-min)
	Z := min + rand.Float32()*(max-min)
	return Normalize(NewVector3(X, Y, Z))
}
