package socketserver

import "net"

type SockServer struct {
	Listener   net.Listener
	ClientPool map[net.Conn]int
}

func (S *SockServer) StartListener() error {
	var err error
	S.Listener, err = net.Listen("tcp", "127.0.0.1:10111")
	if err != nil {
		return err
	}
	return nil
}

func (S *SockServer) CloseListener() error {
	err := S.Listener.Close()
	if err != nil {
		return err
	}
	return nil
}

func (S *SockServer) CloseServer() error {
	err := S.CloseListener()
	if err != nil {
		return err
	}
	for conn := range S.ClientPool {
		err = S.ForceDisconnect(conn)
		if err != nil {
			return err
		}
	}
	return nil
}

func (S *SockServer) AcceptConnection() error {
	conn, err := S.Listener.Accept()
	if err != nil {
		return err
	}

	S.ClientPool[conn] = 1
	return nil
}

func (S *SockServer) ForceDisconnect(conn net.Conn) error {
	err := conn.Close()
	if err != nil {
		return err
	}
	delete(S.ClientPool, conn)
	return nil
}
