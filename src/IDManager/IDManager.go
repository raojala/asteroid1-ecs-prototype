package IDManager

import (
	"github.com/google/uuid"
)

// NewID : abstraction for generating unique integers for entities.
func NewID() uint32 {
	return uuid.New().ID()
}

// // GetIdFromTypeName : returns unique uint32 number from a given string.
// // uses fnv1a algorith to generate a number value from string.
// func GetIdFromTypeName(typeName string) uint32 {
// 	// read somewhere that fnv1a has 1:4000000000 collision rate which is why it should suffice for couple thousand entity type names.
// 	h := fnv.New32a()
// 	h.Write([]byte(typeName))
// 	return h.Sum32()
// }
