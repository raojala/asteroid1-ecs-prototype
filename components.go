package main

const TRANSFORM_TYPE_ID = 1
const CIRCLE_COLLIDER_TYPE_ID = 2

// // Transform : A location, rotation, scale and direction of an entity
// type Transform struct {
// 	Location, Rotation, Scale, Direction vectormath.Vector3
// }

// // CircleCollider : Basic Circle Collider
// type CircleCollider struct {
// 	Radius float32
// 	Center vectormath.Vector3
// }
