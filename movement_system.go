package main

import (
	"test/src/Protocol"
	vectormath "test/src/VectorMath"
)

type MovementSystem struct {
	RegisteredEntities []uint32
}

var MSystem *MovementSystem = RegisterMovementSystem()

func RegisterMovementSystem() *MovementSystem {
	M := &MovementSystem{}
	RegisterSystem(M)
	return M
}

func (MS *MovementSystem) RegisterEntity(entityID uint32) {
	MS.RegisteredEntities = append(MS.RegisteredEntities, entityID)
}

func (MS *MovementSystem) Update(E *Protocol.Ecs) {

	for _, entityHandle := range MS.RegisteredEntities {

		t := E.Transforms[entityHandle]
		newPos := &Protocol.Vector3{}
		newPos.X = t.Location.X * DeltaTime * 250000
		newPos.Y = t.Location.Y * DeltaTime * 250000
		newPos.Z = t.Location.Z * DeltaTime * 250000
		t.Location = vectormath.AddVectors(t.Location, newPos)
	}
}
